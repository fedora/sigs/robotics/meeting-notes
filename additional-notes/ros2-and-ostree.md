# ROS2 and Ostree

### Main Idea
- We forsee a future of robotics software development using container workflows
- Containers can provide many benefits to robotics development using ROS2:
    - Standardized environment which guarantees code can be run and reproduced on any system, allowing for both ease of development and production deployment
    - Many people are already familar with Containerfiles and can, therefore, leverage their existing knowledge and experience 

### The Problem with Containers and Docker
1. The ROS2 container image is built on top of an Ubuntu base image with ROS2 layered on top
    - Creates unnecessary overhead when running a docker container image and defeats the purpose of a container not being the same as a VM
2. GPIO pins needs to be accessible to the docker container
    - Robots need GPIO pins for sensors, actuators, etc
    - Docker containers are meant to be isolated from the host system, meaning these ports need to be manually exposed to the container or a user can just run a privileged container (which poses a security risk)

### Proposal
- Employ the use of native containers/bootable container images for ROS2 development
    - Ostree based systems such as Fedora CoreOS or Fedora Silverblue allow you to rebase your system to use a container image as your source of truth instead of an ostree commit
    - By creating a container image based on Fedora CoreOS or Fedora Silverblue with ROS2 layered on top, the container image acts as instructions for how your OS is modifed (rather than being run in a container)
    - Since it modifies the OS itself, there is no need to build the image on top of Ubuntu
    - Running ROS2 directly on the Fedora CoreOS or Silverblue system also eliminates the problems associated with GPIO pin access 
    - Still reap the benefits of working with container images (layering, reproducibility, ese of deployment, etc)

### Additional Benefits to an Ostree Based System
- Immutable operating system which provides protection against security breaches, an important consideration in the context of robotics
- Fedora CoreOS keeps a version of the previous system when an update occurs, allowing you to rollback to the previous version if the update breas the system
    - Useful for deployed robots that need to minimize downtime
    - Currently, zincati is used to automatically update ostree-based systems and is not compatible with container native based system. However, more investigation on supporting automatic updates might be valuable.
- FCOS has rolling relases which fits the rolling releases of ROS2 Rolling Ridley


### Current Progress:
- A Fedora Silverblue container image with ROS2 layered on top can be successfully built and rebased to. ROS2 `talker` and `listener` demo nodes work without any additional configuration
    - Might need to look into alternatives to storing ROS2 in `/etc` folder since we can't use `/var` (contains user data unique for each system)
- A Fedora CoreOS container image with ROS2 layered on top can be successfully built, but cannot successfully be rebased to yet.
    - Might need to investigate having a GUI for FCOS for `rviz` or `gazebo` so that we don't need to rely on using Silverblue for development/simulation and can just use FCOS for both develoment and production
