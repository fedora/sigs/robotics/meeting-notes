## Glossary

* `Platform`: A target operating system and architecture (e.x. Fedora 38 x86_64, Ubuntu 22.04 amd64, Windows 11 amd64)
* `ROS Distribution`: A release of ROS or ROS 2 (e.x. Noetic, Humble, Rolling)

## Problem Statement

The ROS package release process is not designed to handle changes to the platform list after the ROS distribution has already been released. This is exacerbated by the federated nature of ROS packaging, wherein package maintainers control the source repositories and also the repositories which house the packaging metadata.

Requesting that all package maintainers take action to support a new platform (such as a new release of Fedora) is not feasible, but very seldom are is the packaging metadata actually different between releases of the same platform.

## Proposed Solutions

### Fedora CoreOS Stable Stream

* https://docs.fedoraproject.org/en-US/fedora-coreos/update-streams
* Use Fedora CoreOS stable instead of the default Fedora containers to build RPMS since it has a rolling stable container release
* Avoids the problem of dealing with short releases since it's base on stream updates
* Needs further investigation but looks promissing
* A ROS2 container could be added as a ostree layer from a "ostree native" container image/
    * https://github.com/containers/bootc

### OSBuild [Spin] Robotics Image

* Use OSBuild to build a "Fedora Robotics Spin" from scratch
    * It still needs to pull some RPM packages from a remote repository 
* It would build an OS Image from scratch and could have it's own release cycle
* It might need specific Koji Tags to handle specific package versioning
* More flexibility but more work as well
* OSBuild can export an image defintion to different formats (qcow2, raw, oci, etc) from the same "source"
* Such scheme is already used in the CentOS Stream Automotive SIG to create its images which uses both CentOS Stream and its own RPM repositories
* A possible output could be a base container image for ROS' build system to run its package building

### Fallback Branches

1. Extend [REP 143](http://ros.org/reps/rep-0143.html) to support the specification of "fallback" distributions. When a ROS source package is built, rather than blindly assuming the target branch within the "release" repository, probe for the target branch and fall back to other branches based on newly defined values in REP 143. Note that REP 143 must support platforms which don't have predictable release patterns, so we must treat the Fedora release number as a string and can't just fall back to the `n - 1` release.
   * In addition to changes to the REP and adding the Fedora fallback branches (38 -> 37 -> 36 etc), this would require changes to the [ros_buildfarm](https://github.com/ros-infrastructure/ros_buildfarm) repository to support the branch probing process.
