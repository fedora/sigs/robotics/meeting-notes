# 2023-08-03 Fedora Robotics SIG Meeting

## Topics

* Packaging
    * ignition-common review: https://discussion.fedoraproject.org/t/review-request-ignition-common/85790
* Robotics old content
    * https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0/-/issues/242
* ROS2 + CoreOS + BootC
    * https://github.com/odra/rostree
    * https://github.com/containers/bootc
    * It will use a container image as a layer to be added on top of a ostree based distro (CoreOS/Silverblue in this case)
        * Not running in a continer per say but just using a container as delivery/deployment mechanism
    * Will be built from source short term but ROS could be installed via RPMs long term (rolling release)
* Fallback Branches Document
    * https://hackmd.io/EzfBM0N3TVCKOaJSfYUDIA
    * Rename document so it's more generic
    * Get feedback/ideas for the proposed solution within ROS side
    * Fedora CoreOS stable instead?
* Other
    * 
