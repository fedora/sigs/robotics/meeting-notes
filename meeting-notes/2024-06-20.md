# 2024-06-20 Fedora Robotics SIG Meeting

## Topics

* New Members
* Packaging
    * https://bugzilla.redhat.com/show_bug.cgi?id=1225692
    * https://bugzilla.redhat.com/show_bug.cgi?id=2270953
* Autoware (Open AD-Kit) / ROS2 / OSTree (CoreOS/Silverblue)
    * Autoware contributions:
        * https://github.com/orgs/autowarefoundation/discussions/4897
    * OCP demo
        * https://github.com/awf-ocp-demo
    * Bootc Fedora ROS2 image
    * rhel builds enabled by default in https://ci.ros2.org
        * might be listed as T1 supported platform in the next ROS2 distro release
* O3DE
    * Interest in adding Fedora in their CI build system (RPM packages)
* Other

## Action items
