# 2024-08-01 Fedora Robotics SIG Meeting

## Topics

* New Members
* Packaging
    * https://bugzilla.redhat.com/show_bug.cgi?id=1225692
    * https://bugzilla.redhat.com/show_bug.cgi?id=2270953
* Autoware (Open AD-Kit) / ROS2 / OSTree (CoreOS/Silverblue)
    * ROS2 improving automated checks (and merging) for its patches
        * reduces review latency for said patches 
* O3DE
    * New release end of September or early of October
    * Looking into building their software for Fedora using their own infrastructure
        * Use containers instead of rpms?
        * https://github.com/loherangrin/o3tanks (community initiative)
* Other

## Action items

* Take a look at building O3DE using containers (Fedora base image)