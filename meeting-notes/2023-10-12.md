# 2023-10-12 Fedora Robotics SIG Meeting

## Topics

* New Members
* Packaging
    * O3DE new release: https://docs.o3de.org/docs/release-notes/2310-0-release-notes/
    * https://robotec.ai/ working on O3DE -> ROS integration
    * Packages being built on: https://gitlab.com/fedora/sigs/robotics/rpms
* Autoware (Open AD-Kit) / ROS2 / OSTree (CoreOS/Silverblue)
    * https://github.com/orgs/autowarefoundation/discussions/3651#discussioncomment-7260043
* ROS2 and Fedora
    * Initial work to deploy ROS' build system
* Other

## Action items
