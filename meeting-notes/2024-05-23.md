# 2024-05-23 Fedora Robotics SIG Meeting

## Topics

* New Members
* Packaging
    * https://bugzilla.redhat.com/show_bug.cgi?id=1225692
    * https://bugzilla.redhat.com/show_bug.cgi?id=2270953
* Autoware (Open AD-Kit) / ROS2 / OSTree (CoreOS/Silverblue)
    * Using the new CS9 container to build AWF Open Ad Kit
    * Will demo Openshift CI capabilities to the AWF team next week
    * RHEL might become a tier 1 supported platform
    * New release has 900 RHEL packages
* Other
    * low meeting quorum

## Action items
