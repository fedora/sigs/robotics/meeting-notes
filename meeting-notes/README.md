# Fedora Robotics SIG Meeting Notes

We meet every other Thurday at 4PM UTC: https://calendar.fedoraproject.org/SIGs/

All meeting notes are also posted in Fedora Dicussion: https://discussion.fedoraproject.org/tag/robotics-sig

Index for the SIG's meetings.

* [2023-08-31](./2023-08-31.md)
* [2023-06-08](./2023-06-08.md)
* [2023-05-11](./2023-05-11.md)
* [2023-05-25](./2023-05-25.md)
* [2023-08-03](./2023-08-03.md)
* [2023-07-20](./2023-07-20.md)
* [2023-09-14](./2023-09-14.md)
* [2023-06-12](./2023-06-12.md)
* [2023-08-17](./2023-08-17.md)
