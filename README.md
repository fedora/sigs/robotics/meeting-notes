# Fedora Robotics SIG Meeting Notes

This repository holds the Fedora Robotics SIG meeting notes and related documents:

* [meeting-notes](./meeting-notes): contains the SIG's recurring meeting notes;
* [additional-notes](./additional-notes): contains additional documents and notes for specific discussions.

## License

[MIT](./LICENSE)
